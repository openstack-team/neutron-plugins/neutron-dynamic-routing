#!/usr/bin/make -f

UPSTREAM_GIT = https://github.com/openstack/neutron-dynamic-routing.git
include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3

override_dh_auto_clean:
	python3 setup.py clean
	rm -f debian/*.init debian/*.service debian/*.upstart
	rm -rf build *.egg-info
	rm -f debian/neutron-dynamic-routing-common.config debian/neutron-dynamic-routing-common.postinst
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_clean:
	dh_clean
	find . -type f -name "*.pyc" -delete
	rm -rf build

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func neutron-dynamic-routing-common.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func neutron-dynamic-routing-common.postinst

override_dh_auto_install:
	echo "Do nothing..."

override_dh_install:
	set -e ; for pyvers in $(PYTHON3S); do \
		python$$pyvers setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp; \
	done

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	pkgos-dh_auto_test --no-py2 'neutron_dynamic_routing\.tests.unit\.(?!(.*services\.bgp.agent\.test_bgp_dragent\.TestBgpDrAgent\.test_bgp_dragent_manager.*))'
endif

	mkdir -p $(CURDIR)/debian/neutron-dynamic-routing-common/etc/neutron
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/neutron-dynamic-routing-common/etc/neutron/bgp_dragent.ini \
		--wrap-width 140 \
		--namespace bgp.agent

	mkdir -p $(CURDIR)/debian/neutron-dynamic-routing-common/etc/neutron/policy.d
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslopolicy-sample-generator \
		--output-file $(CURDIR)/debian/neutron-dynamic-routing-common/etc/neutron/policy.d/dynamic_routing.json \
		--format json \
		--namespace neutron-dynamic-routing

	rm -rf $(CURDIR)/debian/tmp/usr/etc

	# Fix bgp_speaker_driver
	# neutron_dynamic_routing.services.bgp.agent.driver.os_ken.driver.OsKenBgpDriver is currently only one and should be default
	pkgos-fix-config-default $(CURDIR)/debian/neutron-dynamic-routing-common/etc/neutron/bgp_dragent.ini bgp bgp_speaker_driver neutron_dynamic_routing.services.bgp.agent.driver.os_ken.driver.OsKenBgpDriver

	dh_install
	dh_missing --fail-missing

override_dh_auto_test:
	echo "Do nothing..."

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
